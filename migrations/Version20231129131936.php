<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129131936 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE choix CHANGE `force` puissance INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licorne CHANGE `force` puissance INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE choix CHANGE puissance `force` INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licorne CHANGE puissance `force` INT NOT NULL');
    }
}
