<?php

namespace App\Controller;

use App\Entity\Licorne;
use App\Entity\Leaderboard;
use App\Repository\LicorneRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\LeaderboardRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LeaderboardController extends AbstractController
{
    // PAGE REUSSIT + BOUTON ENVOIE VERS LEADERBOARD VICTOIRE
    #[Route('/{id}/leaderboard', name: 'app_leaderboard')]
    public function leaderboard(Licorne $id, LicorneRepository $licorne): Response
    {
        $maLicorne = $licorne->find($id);

        return $this->render('licorne/victoire.html.twig', [
            'licorne' => $maLicorne,
        ]);
    }

    #[Route('/leaderboard/{oldpath?retour}', name: 'app_leaderboard_show')]
    public function leaderboardShow(LeaderboardRepository $licorne, LicorneRepository $licorneRepository, string $oldpath): Response
    {
       
        $mesLicornes = $licorne->findAll();

        return $this->render('leaderboard/index.html.twig', [
            'leaderboards' => $mesLicornes,
            'licornes' => $licorneRepository->findAll(),
            'reussite' => "Liste des licornes transcendées",
            'old' => $oldpath
        ]);
    }
}
