<?php

namespace App\Controller;

use App\Repository\ChoixRepository;
use App\Repository\LicorneRepository;
use App\Repository\PlaneteRepository;
use App\Repository\ScenarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Game2Controller extends AbstractController
{
    #[Route('/game2/start', name: 'app_game2_start')]
    public function start(LicorneRepository $licorneRepository, PlaneteRepository $planeteRepository, ScenarioRepository $scenarioRepository, ChoixRepository $choixRepository): Response
    {
        return $this->render('game2/index.html.twig', [
            'controller_name' => 'GameController',
            'licornes' => $licorneRepository->findAll(),
            'planetes' => $planeteRepository->findAll(),
            'scenarios' => $scenarioRepository->findAll(),
            'choixes' => $choixRepository->findAll(),
        ]);
    }
}
