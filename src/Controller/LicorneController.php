<?php

namespace App\Controller;

use App\Entity\Licorne;
use App\Form\LicorneType;
use App\Entity\Leaderboard;
use App\Repository\ChoixRepository;
use App\Repository\LicorneRepository;
use App\Repository\PlaneteRepository;
use App\Repository\ScenarioRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\LeaderboardRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/licorne')]
class LicorneController extends AbstractController
{
    #[Route('/', name: 'app_licorne_index', methods: ['GET'])]
    public function index(LicorneRepository $licorneRepository): Response
    {
        return $this->render('licorne/index.html.twig', [
            'licornes' => $licorneRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_licorne_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $licorne = new Licorne();
        $form = $this->createForm(LicorneType::class, $licorne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($licorne);
            $entityManager->flush();

            return $this->redirectToRoute('app_licorne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('licorne/new.html.twig', [
            'licorne' => $licorne,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_licorne_show', methods: ['GET'])]
    public function show(Licorne $licorne): Response
    {
        return $this->render('licorne/show.html.twig', [
            'licorne' => $licorne,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_licorne_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Licorne $licorne, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(LicorneType::class, $licorne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_licorne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('licorne/edit.html.twig', [
            'licorne' => $licorne,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_licorne_delete', methods: ['POST'])]
    public function delete(Request $request, Licorne $licorne, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$licorne->getId(), $request->request->get('_token'))) {
            $entityManager->remove($licorne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_licorne_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/choix/{pv}/{pvgagnes}/{endurance}/{puissance}/{intelligence}/{esquive}/{p?0}', name: 'app_licorne_consequence_choice', requirements: ['pv' => '\d+','pvgagnes' => '\d+', 'endurance' => '\d+', 'puissance' => '\d+', 'intelligence' => '\d+', 'esquive' => '\d+'], methods: ['GET','POST'])]
    public function ConsequenceChoice(
        Licorne $id,
        int $pv,
        int $pvgagnes,
        int $endurance,
        int $puissance,
        int $intelligence,
        int $esquive,
        int $p,
        EntityManagerInterface $entityManager,
        LicorneRepository $licorneRepository,
        PlaneteRepository $planeteRepository,
        ScenarioRepository $scenarioRepository,
        ChoixRepository $choixRepository
    ): Response
    {
        $maLicorne = $licorneRepository->find($id);

        $maLicorne->setPdv($maLicorne->getPdv()-($pv));
        $maLicorne->setPdv($maLicorne->getPdv()+($pvgagnes));
        $maLicorne->setEndurance($maLicorne->getEndurance()+($endurance));
        $maLicorne->setPuissance($maLicorne->getPuissance()+($puissance));
        $maLicorne->setIntelligence($maLicorne->getIntelligence()+($intelligence));
        $maLicorne->setEsquive($maLicorne->getEsquive()+($esquive));

        $entityManager->persist($maLicorne);
        $entityManager->flush();
        if($p >= 2){
            return $this->redirectToRoute('app_licorne_victory',[
                'nom' => $maLicorne->getNom()
            ]);
        }
        return $this->render('game2/index.html.twig', [
            'licornes' => $licorneRepository->findAll(),
            'planetes' => $planeteRepository->findAll(),
            'scenarios' => $scenarioRepository->findAll(),
            'choixes' => $choixRepository->findAll(),
            'diffpv' => $pv,
            'diffpvm' => $pvgagnes,
            'diffendurance' => $endurance,
            'diffpuissance' => $puissance,
            'diffintelligence' => $intelligence,
            'diffesquive' => $esquive,
            'p' => $p+1
        ]);
    }

    // AFFICHAGE DU LEADERBOARD
    #[Route('/route/victoire/{nom}', name: 'app_licorne_victory', methods: ['GET'])]
    public function victoire(string $nom, EntityManagerInterface $entityManager, LeaderboardRepository $leaderRepo, LicorneRepository $licorneRepository): Response
{
        // ajouter notre nom licorne dans leaderboard
        // Créer notre entity leaderboard:
        $newNomLeaderBoard = new Leaderboard();
        // ajoutez notre nom de poney dans leaderboard
        $newNomLeaderBoard->setNom($nom);
        //persist et flush via le manager:
        $entityManager->persist($newNomLeaderBoard);
        $entityManager->flush();
        //dd('test');
        //récupérer TOUTES les licornes de leaderBoard dans $licornes, puis envoie dans le template
        $licornes = $leaderRepo->findAll();
        return $this->render('leaderboard/index.html.twig', [
            'leaderboards' => $licornes,
            'licornes' => $licorneRepository->findAll(),
            'reussite' => "FELICITATION !!!!!",
            'old' => 'retour'
        ]);
    }
}
