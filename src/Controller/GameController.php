<?php

namespace App\Controller;

use App\Repository\ChoixRepository;
use App\Repository\LicorneRepository;
use App\Repository\PlaneteRepository;
use App\Repository\ScenarioRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GameController extends AbstractController
{
    #[Route('/game', name: 'app_game')]
    public function index(LicorneRepository $licorneRepository, Request $request): Response
    {
        
        $test = substr($request->getPathInfo(), 1, 10);
        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'licornes' => $licorneRepository->findAll(),
            'request' => $test
        ]);
    }

    #[Route('/game/start', name: 'app_game_start')]
    public function start(LicorneRepository $licorneRepository, PlaneteRepository $planeteRepository, ScenarioRepository $scenarioRepository, ChoixRepository $choixRepository): Response
    {
        return $this->render('game/start.html.twig', [
            'controller_name' => 'GameController',
            'licornes' => $licorneRepository->findAll(),
            'planetes' => $planeteRepository->findAll(),
            'scenarios' => $scenarioRepository->findAll(),
            'choixes' => $choixRepository->findAll(),
        ]);
    }
}
