<?php

namespace App\Controller;

use App\Repository\LicorneRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestController extends AbstractController
{
    #[Route('/test/{id}', name: 'app_test')]
    public function index(LicorneRepository $licorne): Response
    {   
        dd($licorne);
        return $this->render('test/index.html.twig', [
            'licornes' => $licorne,
        ]);
    }
}
