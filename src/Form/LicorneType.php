<?php

namespace App\Form;

use App\Entity\Licorne;
use App\Repository\LicorneRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class LicorneType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nom',
                ],
            ])
            ->add('pdv', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Point(s) de vie',
                    'hidden' => true,
                ],
                'data' => 10,
            ])
            ->add('endurance', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Point(s) d\'endurance',
                ],
            ])
            ->add('puissance', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Point(s) de puissance',
                ],
            ])
            ->add('intelligence', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Point(s) d\'intelligence',
                ],
            ])
            ->add('esquive', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Point(s) d\'esquive',
                ],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, [$this, 'supprimerLicornes'])
            ->addEventListener(FormEvents::POST_SUBMIT, [$this, 'validateTotalPoints']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Licorne::class,
        ]);
    }

    public function supprimerLicornes(FormEvent $event): void
    {
        $licorne = $event->getData();
        $licorneRepository = $this->entityManager->getRepository(Licorne::class);
        $licornes = $licorneRepository->findAll();

        foreach ($licornes as $uneLicorne) {
            $this->entityManager->remove($uneLicorne);
        }

        $this->entityManager->flush();
    }

    public function validateTotalPoints(FormEvent $event): void
    {
        $licorne = $event->getData();
        $form = $event->getForm();

        $totalPoints = $licorne->getEndurance() + $licorne->getPuissance() + $licorne->getIntelligence() + $licorne->getEsquive();

        if ($totalPoints > 5) {
            $form->get('endurance')->addError(new FormError('Le total des points ne peut pas dépasser 5.'));
        }
    }
}
