<?php

namespace App\Entity;

use App\Repository\ChoixRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChoixRepository::class)]
class Choix
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?int $pv_perdus = null;

    #[ORM\Column(nullable: true)]
    private ?int $pv_gagnes = null;

    #[ORM\Column(nullable: true)]
    private ?int $endurance = null;

    #[ORM\Column(nullable: true)]
    private ?int $puissance = null;

    #[ORM\Column(nullable: true)]
    private ?int $intelligence = null;

    #[ORM\Column(nullable: true)]
    private ?int $esquive = null;

    #[ORM\ManyToOne(inversedBy: 'choix')]
    private ?Scenario $scenario = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPvPerdus(): ?int
    {
        return $this->pv_perdus;
    }

    public function setPvPerdus(?int $pv_perdus): static
    {
        $this->pv_perdus = $pv_perdus;

        return $this;
    }

    public function getPvGagnes(): ?int
    {
        return $this->pv_gagnes;
    }

    public function setPvGagnes(?int $pv_gagnes): static
    {
        $this->pv_gagnes = $pv_gagnes;

        return $this;
    }

    public function getEndurance(): ?int
    {
        return $this->endurance;
    }

    public function setEndurance(?int $endurance): static
    {
        $this->endurance = $endurance;

        return $this;
    }

    public function getPuissance(): ?int
    {
        return $this->puissance;
    }

    public function setPuissance(?int $puissance): static
    {
        $this->puissance = $puissance;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(?int $intelligence): static
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getEsquive(): ?int
    {
        return $this->esquive;
    }

    public function setEsquive(?int $esquive): static
    {
        $this->esquive = $esquive;

        return $this;
    }

    public function getScenario(): ?Scenario
    {
        return $this->scenario;
    }

    public function setScenario(?Scenario $scenario): static
    {
        $this->scenario = $scenario;

        return $this;
    }

    public function __toString(): string
    {
        return $this->nom;
    }
}
