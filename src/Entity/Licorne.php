<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\LicorneRepository;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: LicorneRepository::class)]
class Licorne
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $pdv = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column]
    private ?int $endurance = null;

    #[ORM\Column]
    private ?int $puissance = null;

    #[ORM\Column]
    private ?int $intelligence = null;

    #[ORM\Column]
    private ?int $esquive = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPdv(): ?int
    {
        return $this->pdv;
    }

    public function setPdv(int $pdv): static
    {
        $this->pdv = $pdv;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEndurance(): ?int
    {
        return $this->endurance;
    }

    public function setEndurance(int $endurance): static
    {
        $this->endurance = $endurance;

        return $this;
    }

    public function getPuissance(): ?int
    {
        return $this->puissance;
    }

    public function setPuissance(int $puissance): static
    {
        $this->puissance = $puissance;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(int $intelligence): static
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getEsquive(): ?int
    {
        return $this->esquive;
    }

    public function setEsquive(int $esquive): static
    {
        $this->esquive = $esquive;

        return $this;
    }

    public function __toString(): string
    {
        return $this->nom;
    }

    public function isValidTotalPoints(ExecutionContextInterface $context)
    {
        $totalPoints = $this->endurance + $this->puissance + $this->intelligence + $this->esquive;

        if ($totalPoints > 5) {
            $context->buildViolation('Le total des points ne peut pas dépasser 5.')
                ->atPath('endurance')
                ->addViolation();
        }
    }
}
