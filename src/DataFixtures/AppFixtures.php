<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Choix;
use App\Entity\Planete;
use App\Entity\Scenario;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $hasher)
    {
    }
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword(
            $this->hasher->hashPassword(
                $user, "admin"
            )
        );
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $Planete1 = new Planete();
        $Planete1->setNom("Licornia");
        $Planete1->setDescription("Bienvenue sur la planète Licornia, le joyau scintillant de l'univers où la magie et la féérie se mêlent dans un ballet éternel de couleurs chatoyantes. À votre arrivée, une douce lueur éthérée enveloppe votre corps, tandis que des arcs-en-ciel dansent dans le ciel, créant un spectacle envoûtant d'éclats de lumière.
        Les terres de Licornia s'étendent à perte de vue, avec des prairies luxuriantes ornées de fleurs phosphorescentes qui émettent un doux éclat. Les arbres majestueux portent des feuilles en forme de cœurs étincelants, et les rivières cristallines murmurent des mélodies harmonieuses en coulant à travers des vallées enchantées.
        Des créatures magiques saluent votre arrivée, des papillons lumineux aux chants enchanteurs, exprimant la bienvenue dans ce royaume extraordinaire. Les montagnes lointaines arborent des sommets couronnés de cristaux magiques, et au loin, un château féerique se dresse, où réside le mystérieux Conseil des Licornes.
        Alors que vous prenez vos premiers pas sur le sol doux de Licornia, la terre résonne de vibrations magiques, chacune vous invitant à découvrir les mystères et les merveilles qui vous attendent. Vous êtes désormais face à des choix cruciaux, et l'aventure magique de Licornia attend que vous la guidiez avec sagesse. Que la lumière de votre corne brille intensément dans ce monde féerique, où chaque décision est une étincelle de destin.");

        $Planete2 = new Planete();
        $Planete2->setNom("Prismara");
        $Planete2->setDescription("Bienvenue sur la planète Prismara, un lieu éblouissant où la lumière et la couleur fusionnent dans une symphonie éclatante. À votre arrivée, une aurore boréale permanente illumine le ciel, créant un kaléidoscope hypnotique de teintes chatoyantes.
        Les terres de Prismara sont composées de cristaux géants qui émettent une lueur douce et constante. Les plaines étincelantes sont tapissées de cristaux aux reflets changeants, créant un paysage étincelant qui s'étend à perte de vue. Des rivières de lumière liquide serpentent à travers des vallées, réfractant les rayons du soleil dans des arcs-en-ciel éblouissants.
        Des créatures cristallines, telles que les lucioles prismatiques et les papillons de diamant, accueillent chaque visiteur avec des danses lumineuses. Les arbres, dotés de feuilles translucides, produisent une douce mélodie lorsque la brise les caresse, créant une ambiance harmonique et apaisante.
        Au sommet des montagnes cristallines, des édifices majestueux sculptés dans des gemmes iridescentes se dressent, annonçant la présence du Conseil des Prismes, gardiens de la sagesse et des secrets de Prismara.
        Alors que vous posez les pieds sur le sol scintillant de Prismara, vous ressentez une énergie vibrante qui transcende la réalité. Devant vous se présentent des choix qui détermineront votre parcours à travers ce monde resplendissant. Que la clarté de votre destinée se révèle dans cette terre de prismes et de possibilités infinies. Que votre aventure commence, explorateur de Prismara !");

        $Planete3 = new Planete();
        $Planete3->setNom("Féélandia");
        $Planete3->setDescription("Bienvenue sur la planète Féélandia, un royaume magique où les rêves prennent vie et la féérie règne en maître. Dès votre arrivée, une pluie de poussière d'étoiles s'abat doucement, créant une atmosphère éthérée et enchantée.
        Féélandia est un patchwork de paysages fantastiques : des forêts d'arbres en forme de lanternes aux fleurs qui s'ouvrent en musique, en passant par des vallées de neige poudreuse qui scintillent comme des cristaux. Les rivières murmurent des contes d'antan et les montagnes semblent être des gardiennes bienveillantes, veillant sur ce royaume féerique.
        Des créatures magiques, telles que des fées multicolores, des lutins farceurs et des licornes aux crinières chatoyantes, saluent chaque visiteur avec une danse joyeuse. Des arcs-en-ciel permanents traversent le ciel, unissant les cieux et la terre dans une palette de couleurs éclatantes.
        Au centre de Féélandia se dresse le Palais des Songes, un édifice majestueux qui brille de mille feux. C'est là que réside la Reine des Fées et son Conseil Magique, prêts à guider ceux qui aspirent à vivre des aventures enchantées.
        Alors que vous explorez les contrées féériques de Féélandia, chaque pas déclenche une réaction magique, chaque souffle transporte un doux murmure de secrets anciens. Devant vous s'ouvrent des chemins mystiques, et vos choix détermineront le récit de votre conte de fées personnel dans cette terre où l'impossible devient réalité. Que la magie de Féélandia illumine votre voyage, explorateur des merveilles féeriques !");

        $Scenario2 = new Scenario();
        $Scenario2->setNom("Bruit dans l'angle mort");
        $Scenario2->setPlanete($Planete1);
        $Scenario2->setDescription("En vous promenant à travers la forêt enchantée de Licornia, vous percevez soudainement un bruit mystérieux. Une mélodie douce, mais étrangement familière, résonne entre les arbres majestueux. La licorne se demande si elle devrait s'aventurer pour découvrir l'origine de ce son enchanteur ou rester à l'abri des regards, méfiante face à l'inconnu.");

        $Scenario3 = new Scenario();
        $Scenario3->setNom("Un dragon arc-en-ciel mystérieux");
        $Scenario3->setPlanete($Planete1);
        $Scenario3->setDescription("Alors que vous traversez une prairie scintillante, un frémissement tellurique attire votre attention. Un dragon majestueux, aux écailles chatoyantes comme un arc-en-ciel, émerge du sol. Il vous observe avec curiosité. Vous devez maintenant décider si vous allez tenter une approche pacifique ou vous préparer à défendre Licornia au cas où cette créature magique serait une menace.");

        $Scenario4 = new Scenario();
        $Scenario4->setNom("Rencontre avec une créature des rivières cristallines");
        $Scenario4->setPlanete($Planete1);
        $Scenario4->setDescription("En suivant le doux murmure d'une rivière cristalline, vous découvrez une créature aquatique éthérée. Ses yeux étincellent comme des étoiles. Vous êtes maintenant face à un choix : engager une conversation avec cette créature magique ou décider de ne pas perturber son environnement paisible.");

        $Scenario5 = new Scenario();
        $Scenario5->setNom("Trouver une clairière enchantée");
        $Scenario5->setPlanete($Planete1);
        $Scenario5->setDescription("Au détour d'un sentier sinueux, vous arrivez dans une clairière magique. Des fleurs phosphorescentes forment un tapis brillant, et une aura paisible emplit l'air. Vous pouvez choisir de vous installer pour profiter de la sérénité de cet endroit mystique ou d'explorer davantage pour dévoiler les secrets cachés de la clairière.");

        $Scenario6 = new Scenario();
        $Scenario6->setNom("L'Éclipse Magique approche");
        $Scenario6->setPlanete($Planete1);
        $Scenario6->setDescription("Une nouvelle se répand dans tout Licornia : une éclipse magique menace de plonger la planète dans l'obscurité éternelle. Les licornes du Conseil se tournent vers vous pour rassembler des artefacts magiques dispersés à travers le royaume. Votre choix d'agir ou de chercher des conseils aura des conséquences importantes.");

        $Scenario7 = new Scenario();
        $Scenario7->setNom("Rencontre avec un papillon égaré");
        $Scenario7->setPlanete($Planete1);
        $Scenario7->setDescription("Alors que vous volez à travers une vallée de fleurs arc-en-ciel, un papillon lumineux s'approche de vous, semblant perdu. Vous pouvez décider de consacrer du temps à aider cette créature à retrouver son chemin ou de continuer votre vol sans vous attarder.");

        $Scenario8 = new Scenario();
        $Scenario8->setNom("Les Lumières dansantes de Prismara");
        $Scenario8->setPlanete($Planete2);
        $Scenario8->setDescription("Prismara célèbre un spectacle de lumières envoûtant où les cristaux géants émettent des éclats chatoyants dans la nuit. Cependant, une perturbation mystérieuse menace de plonger la planète dans l'obscurité.");
        
        $Scenario9 = new Scenario();
        $Scenario9->setNom("Le Cristal de l'Aurore");
        $Scenario9->setPlanete($Planete2);
        $Scenario9->setDescription("Une légende parle d'un cristal rare, l'Aurore, caché au sommet des montagnes cristallines de Prismara. Les habitants cherchent des courageux explorateurs pour le trouver, mais des créatures magiques gardent jalousement cet artefact précieux.");
        
        $Scenario10 = new Scenario();
        $Scenario10->setNom("La Symphonie des Éclats");
        $Scenario10->setPlanete($Planete2);
        $Scenario10->setDescription("Les éclats de cristal de Prismara sont connus pour produire une mélodie enchanteresse. Cependant, un éclat puissant menace de perturber cette symphonie magique. Les habitants demandent de l'aide pour rétablir l'harmonie cristalline.");
        
        $Scenario11 = new Scenario();
        $Scenario11->setNom("Les Portes de la Réflexion");
        $Scenario11->setPlanete($Planete2);
        $Scenario11->setDescription("Des portes mystiques ont été découvertes, chacune reflétant une réalité différente de Prismara. Les explorateurs sont appelés à choisir une porte, mais chaque réalité présente ses propres défis et mystères.");
        
        $Scenario12 = new Scenario();
        $Scenario12->setNom("La Danse des Étoiles Filantes");
        $Scenario12->setPlanete($Planete2);
        $Scenario12->setDescription("Une pluie d'étoiles filantes illumine le ciel de Prismara, créant un spectacle céleste éblouissant. Cependant, des étoiles tombantes atterrissent mystérieusement sur la planète, apportant avec elles des énergies cosmiques inconnues.");
        
        $Scenario13 = new Scenario();
        $Scenario13->setNom("Les Gardiens des Cristaux Perdus");
        $Scenario13->setPlanete($Planete2);
        $Scenario13->setDescription("Des créatures ancestrales, les Gardiens des Cristaux Perdus, se réveillent et demandent l'aide des explorateurs. Ils ont besoin d'assistance pour retrouver les cristaux égarés dispersés dans les terres de Prismara.");
        
        $Scenario14 = new Scenario();
        $Scenario14->setNom("La Fontaine de Clarté");
        $Scenario14->setPlanete($Planete2);
        $Scenario14->setDescription("Au cœur de Prismara, une fontaine magique offre la clarté éternelle. Cependant, des ombres mystérieuses menacent de contaminer les eaux cristallines. Les habitants recherchent des héros pour purifier la fontaine et sauver la planète.");

        $Scenario15 = new Scenario();
        $Scenario15->setNom("L'Éclat de l'Arc-en-ciel");
        $Scenario15->setPlanete($Planete3);
        $Scenario15->setDescription("Un éclat de l'arc-en-ciel, source de toute la magie de Féélandia, a disparu. Les créatures féeriques sont plongées dans une tristesse profonde. Les explorateurs sont appelés à retrouver cet éclat pour restaurer la joie et la magie.");
        
        $Scenario16 = new Scenario();
        $Scenario16->setNom("La Forêt des Lanternes Éternelles");
        $Scenario16->setPlanete($Planete3);
        $Scenario16->setDescription("Au cœur de Féélandia se trouve la Forêt des Lanternes Éternelles. Cependant, l'une des lanternes magiques s'est éteinte, plongeant la forêt dans l'obscurité. Les explorateurs sont sollicités pour rallumer la lanterne et dissiper les ombres.");
        
        $Scenario17 = new Scenario();
        $Scenario17->setNom("La Vallée des Rêves Endormis");
        $Scenario17->setPlanete($Planete3);
        $Scenario17->setDescription("Une étrange malédiction a plongé les habitants de Féélandia dans un sommeil profond. Les explorateurs doivent trouver un moyen de briser ce charme et réveiller la vallée de rêves endormis.");
        
        $Scenario18 = new Scenario();
        $Scenario18->setNom("Le Labyrinthe des Illusions");
        $Scenario18->setPlanete($Planete3);
        $Scenario18->setDescription("Un labyrinthe mystique a émergé, créant des illusions déroutantes. Les explorateurs sont invités à traverser ce labyrinthe pour découvrir ses secrets et rétablir l'harmonie dans Féélandia.");
        
        $Scenario19 = new Scenario();
        $Scenario19->setNom("Le Vol des Lutins Farceurs");
        $Scenario19->setPlanete($Planete3);
        $Scenario19->setDescription("Les lutins farceurs de Féélandia ont dérobé des objets précieux et les ont dispersés dans tout le royaume. Les explorateurs sont missionnés pour retrouver ces objets et ramener la malice des lutins sous contrôle.");
        
        $Scenario20 = new Scenario();
        $Scenario20->setNom("La Cascade des Souhaits");
        $Scenario20->setPlanete($Planete3);
        $Scenario20->setDescription("La Cascade des Souhaits, source de la puissance magique de Féélandia, est menacée par une force obscure. Les explorateurs sont appelés à protéger la cascade et à réaliser un vœu pour préserver l'équilibre magique.");
        
        $Scenario21 = new Scenario();
        $Scenario21->setNom("Les Portes de l'Infini");
        $Scenario21->setPlanete($Planete3);
        $Scenario21->setDescription("Des portes magiques apparaissent dans différentes parties de Féélandia, ouvrant des passages vers des dimensions inexplorées. Les explorateurs sont encouragés à franchir ces portes et à découvrir les merveilles cachées au-delà.");
        
        $Scenario22 = new Scenario();
        $Scenario22->setNom("La Quête du Trille Enchanté");
        $Scenario22->setPlanete($Planete3);
        $Scenario22->setDescription("Le Trille Enchanté, une mélodie mystique qui maintient l'harmonie à Féélandia, a été perdu. Les explorateurs sont conviés à suivre les notes de cette mélodie pour retrouver l'instrument magique et rétablir la paix musicale.");        

        $Choix4 = new Choix();
        $Choix4->setScenario($Scenario2);
        $Choix4->setNom("S'aventurer vers le bruit pour enquêter");
        $Choix4->setDescription("-");
        $Choix4->setPvPerdus(3);
        $Choix4->setPvGagnes(0);
        $Choix4->setEndurance(2);
        $Choix4->setPuissance(0);
        $Choix4->setIntelligence(0);
        $Choix4->setEsquive(1);

        $Choix5 = new Choix();
        $Choix5->setScenario($Scenario2);
        $Choix5->setNom("Observer discrètement depuis les ombres sans se déplacer");
        $Choix5->setDescription("-");
        $Choix5->setPvPerdus(5);
        $Choix5->setPvGagnes(2);
        $Choix5->setEndurance(0);
        $Choix5->setPuissance(0);
        $Choix5->setIntelligence(2);
        $Choix5->setEsquive(2);

        $Choix6 = new Choix();
        $Choix6->setScenario($Scenario2);
        $Choix6->setNom("Utiliser la magie pour détecter l'origine du son");
        $Choix6->setDescription("-");
        $Choix6->setPvPerdus(2);
        $Choix6->setPvGagnes(0);
        $Choix6->setEndurance(1);
        $Choix6->setPuissance(2);
        $Choix6->setIntelligence(1);
        $Choix6->setEsquive(0);

        $Choix7 = new Choix();
        $Choix7->setScenario($Scenario2);
        $Choix7->setNom("S'éloigner silencieusement du lieu du bruit");
        $Choix7->setDescription("-");
        $Choix7->setPvPerdus(5);
        $Choix7->setPvGagnes(3);
        $Choix7->setEndurance(1);
        $Choix7->setPuissance(0);
        $Choix7->setIntelligence(2);
        $Choix7->setEsquive(1);

        $Choix8 = new Choix();
        $Choix8->setScenario($Scenario3);
        $Choix8->setNom("Tenter de communiquer pacifiquement avec des signaux amicaux");
        $Choix8->setDescription("-");
        $Choix8->setPvPerdus(5);
        $Choix8->setPvGagnes(2);
        $Choix8->setEndurance(0);
        $Choix8->setPuissance(0);
        $Choix8->setIntelligence(3);
        $Choix8->setEsquive(1);   
        
        $Choix9 = new Choix();
        $Choix9->setScenario($Scenario3);
        $Choix9->setNom("Se préparer au combat, adopter une posture défensive");
        $Choix9->setDescription("-");
        $Choix9->setPvPerdus(5);
        $Choix9->setPvGagnes(0);
        $Choix9->setEndurance(1);
        $Choix9->setPuissance(3);
        $Choix9->setIntelligence(0);
        $Choix9->setEsquive(1);    

        $Choix10 = new Choix();
        $Choix10->setScenario($Scenario3);
        $Choix10->setNom("Utiliser la magie pour sonder les intentions du dragon");
        $Choix10->setDescription("-");
        $Choix10->setPvPerdus(5);
        $Choix10->setPvGagnes(0);
        $Choix10->setEndurance(1);
        $Choix10->setPuissance(1);
        $Choix10->setIntelligence(1);
        $Choix10->setEsquive(1);    

        $Choix11 = new Choix();
        $Choix11->setScenario($Scenario3);
        $Choix11->setNom("Chercher une issue de secours discrète sans attirer l'attention");
        $Choix11->setDescription("-");
        $Choix11->setPvPerdus(5);
        $Choix11->setPvGagnes(2);
        $Choix11->setEndurance(1);
        $Choix11->setPuissance(0);
        $Choix11->setIntelligence(3);
        $Choix11->setEsquive(1);    

                // Suite des choix pour le scénario 1 (Rencontre avec une créature des rivières cristallines)
        $Choix12 = new Choix();
        $Choix12->setScenario($Scenario4);
        $Choix12->setNom("Engager la conversation en utilisant un langage magique");
        $Choix12->setDescription("-");
        $Choix12->setPvPerdus(5);
        $Choix12->setPvGagnes(2);
        $Choix12->setEndurance(0);
        $Choix12->setPuissance(0);
        $Choix12->setIntelligence(3);
        $Choix12->setEsquive(1);

        $Choix13 = new Choix();
        $Choix13->setScenario($Scenario4);
        $Choix13->setNom("Continuer son chemin tout en saluant respectueusement la créature");
        $Choix13->setDescription("-");
        $Choix13->setPvPerdus(5);
        $Choix13->setPvGagnes(1);
        $Choix13->setEndurance(0);
        $Choix13->setPuissance(0);
        $Choix13->setIntelligence(2);
        $Choix13->setEsquive(2);

        $Choix14 = new Choix();
        $Choix14->setScenario($Scenario4);
        $Choix14->setNom("Offrir un cadeau magique à la créature comme signe de paix");
        $Choix14->setDescription("-");
        $Choix14->setPvPerdus(5);
        $Choix14->setPvGagnes(2);
        $Choix14->setEndurance(1);
        $Choix14->setPuissance(0);
        $Choix14->setIntelligence(2);
        $Choix14->setEsquive(1);

        $Choix15 = new Choix();
        $Choix15->setScenario($Scenario4);
        $Choix15->setNom("Utiliser la magie pour créer une harmonie entre soi et la créature");
        $Choix15->setDescription("-");
        $Choix15->setPvPerdus(5);
        $Choix15->setPvGagnes(2);
        $Choix15->setEndurance(1);
        $Choix15->setPuissance(0);
        $Choix15->setIntelligence(3);
        $Choix15->setEsquive(1);

        // Suite des choix pour le scénario 1 (Trouver une clairière enchantée)
        $Choix16 = new Choix();
        $Choix16->setScenario($Scenario5);
        $Choix16->setNom("S'installer pour un moment de méditation et de tranquillité");
        $Choix16->setDescription("-");
        $Choix16->setPvPerdus(5);
        $Choix16->setPvGagnes(2);
        $Choix16->setEndurance(0);
        $Choix16->setPuissance(0);
        $Choix16->setIntelligence(2);
        $Choix16->setEsquive(1);

        $Choix17 = new Choix();
        $Choix17->setScenario($Scenario5);
        $Choix17->setNom("Explorer davantage la clairière pour découvrir ses secrets cachés");
        $Choix17->setDescription("-");
        $Choix17->setPvPerdus(2);
        $Choix17->setPvGagnes(0);
        $Choix17->setEndurance(2);
        $Choix17->setPuissance(1);
        $Choix17->setIntelligence(1);
        $Choix17->setEsquive(0);

        $Choix18 = new Choix();
        $Choix18->setScenario($Scenario5);
        $Choix18->setNom("Utiliser la magie pour ressentir l'énergie de la clairière");
        $Choix18->setDescription("-");
        $Choix18->setPvPerdus(5);
        $Choix18->setPvGagnes(0);
        $Choix18->setEndurance(1);
        $Choix18->setPuissance(2);
        $Choix18->setIntelligence(2);
        $Choix18->setEsquive(0);

        $Choix19 = new Choix();
        $Choix19->setScenario($Scenario5);
        $Choix19->setNom("Créer une œuvre magique éphémère pour honorer la clairière");
        $Choix19->setDescription("-");
        $Choix19->setPvPerdus(5);
        $Choix19->setPvGagnes(2);
        $Choix19->setEndurance(1);
        $Choix19->setPuissance(1);
        $Choix19->setIntelligence(2);
        $Choix19->setEsquive(1);

        // Suite des choix pour le scénario 1 (L'Éclipse Magique approche)
        $Choix20 = new Choix();
        $Choix20->setScenario($Scenario6);
        $Choix20->setNom("Rassembler des artefacts magiques pour contrer l'éclipse");
        $Choix20->setDescription("-");
        $Choix20->setPvPerdus(2);
        $Choix20->setPvGagnes(0);
        $Choix20->setEndurance(2);
        $Choix20->setPuissance(1);
        $Choix20->setIntelligence(1);
        $Choix20->setEsquive(0);

        $Choix21 = new Choix();
        $Choix21->setScenario($Scenario6);
        $Choix21->setNom("Chercher des conseils auprès du Conseil des Licornes avant d'agir");
        $Choix21->setDescription("-");
        $Choix21->setPvPerdus(5);
        $Choix21->setPvGagnes(0);
        $Choix21->setEndurance(0);
        $Choix21->setPuissance(0);
        $Choix21->setIntelligence(3);
        $Choix21->setEsquive(1);

        $Choix22 = new Choix();
        $Choix22->setScenario($Scenario6);
        $Choix22->setNom("Explorer les prophéties magiques pour comprendre les conséquences");
        $Choix22->setDescription("-");
        $Choix22->setPvPerdus(5);
        $Choix22->setPvGagnes(0);
        $Choix22->setEndurance(1);
        $Choix22->setPuissance(1);
        $Choix22->setIntelligence(2);
        $Choix22->setEsquive(1);

        $Choix23 = new Choix();
        $Choix23->setScenario($Scenario6);
        $Choix23->setNom("S'entraîner intensivement pour renforcer ses pouvoirs avant l'éclipse");
        $Choix23->setDescription("-");
        $Choix23->setPvPerdus(2);
        $Choix23->setPvGagnes(0);
        $Choix23->setEndurance(2);
        $Choix23->setPuissance(2);
        $Choix23->setIntelligence(0);
        $Choix23->setEsquive(0);

        // Suite des choix pour le scénario 1 (Rencontre avec un papillon égaré)
        $Choix24 = new Choix();
        $Choix24->setScenario($Scenario7);
        $Choix24->setNom("Aider le papillon à retrouver son chemin en utilisant la magie");
        $Choix24->setDescription("-");
        $Choix24->setPvPerdus(5);
        $Choix24->setPvGagnes(2);
        $Choix24->setEndurance(0);
        $Choix24->setPuissance(1);
        $Choix24->setIntelligence(2);
        $Choix24->setEsquive(1);

        $Choix25 = new Choix();
        $Choix25->setScenario($Scenario7);
        $Choix25->setNom("Créer une petite danse magique pour divertir le papillon");
        $Choix25->setDescription("-");
        $Choix25->setPvPerdus(5);
        $Choix25->setPvGagnes(2);
        $Choix25->setEndurance(1);
        $Choix25->setPuissance(0);
        $Choix25->setIntelligence(3);
        $Choix25->setEsquive(1);

        $Choix26 = new Choix();
        $Choix26->setScenario($Scenario7);
        $Choix26->setNom("Observer le comportement du papillon pour déterminer s'il est amical");
        $Choix26->setDescription("-");
        $Choix26->setPvPerdus(5);
        $Choix26->setPvGagnes(1);
        $Choix26->setEndurance(0);
        $Choix26->setPuissance(0);
        $Choix26->setIntelligence(2);
        $Choix26->setEsquive(1);

        $Choix27 = new Choix();
        $Choix27->setScenario($Scenario7);
        $Choix27->setNom("Continuer à voler en suivant le papillon, prêt à réagir à tout changement");
        $Choix27->setDescription("-");
        $Choix27->setPvPerdus(5);
        $Choix27->setPvGagnes(2);
        $Choix27->setEndurance(2);
        $Choix27->setPuissance(0);
        $Choix27->setIntelligence(1);
        $Choix27->setEsquive(2);

        $Choix28 = new Choix();
        $Choix28->setScenario($Scenario8);
        $Choix28->setNom("Médiation Lumineuse");
        $Choix28->setDescription("Tenter de calmer les tensions entre les groupes de lucioles.");
        $Choix28->setPvPerdus(5);
        $Choix28->setPvGagnes(2);
        $Choix28->setEndurance(2);
        $Choix28->setPuissance(0);
        $Choix28->setIntelligence(1);
        $Choix28->setEsquive(2);

        $Choix29 = new Choix();
        $Choix29->setScenario($Scenario8);
        $Choix29->setNom("Spectateur Silencieux");
        $Choix29->setDescription("Observer la situation sans intervenir.");
        $Choix29->setPvPerdus(5);
        $Choix29->setPvGagnes(0);
        $Choix29->setEndurance(1);
        $Choix29->setPuissance(0);
        $Choix29->setIntelligence(2);
        $Choix29->setEsquive(1);

        $Choix30 = new Choix();
        $Choix30->setScenario($Scenario8);
        $Choix30->setNom("Médiation Délicate");
        $Choix30->setDescription("Proposer une médiation entre les deux groupes de lucioles.");
        $Choix30->setPvPerdus(5);
        $Choix30->setPvGagnes(2);
        $Choix30->setEndurance(1);
        $Choix30->setPuissance(0);
        $Choix30->setIntelligence(2);
        $Choix30->setEsquive(1);

        $Choix31 = new Choix();
        $Choix31->setScenario($Scenario8);
        $Choix31->setNom("Consulter les anciens pour des conseils sur la résolution du conflit");
        $Choix31->setDescription("-");
        $Choix31->setPvPerdus(5);
        $Choix31->setPvGagnes(1);
        $Choix31->setEndurance(2);
        $Choix31->setPuissance(0);
        $Choix31->setIntelligence(3);
        $Choix31->setEsquive(1);

        // Choix 5 - Ascension Héroïque
        $Choix32 = new Choix();
        $Choix32->setScenario($Scenario9);
        $Choix32->setNom("Entreprendre l'ascension périlleuse des montagnes pour atteindre le Cristal de l'Aurore");
        $Choix32->setDescription("-");
        $Choix32->setPvPerdus(2);
        $Choix32->setPvGagnes(1);
        $Choix32->setEndurance(2);
        $Choix32->setPuissance(2);
        $Choix32->setIntelligence(0);
        $Choix32->setEsquive(1);

        // Choix 6 - Subjuguer les Gardiens
        $Choix33 = new Choix();
        $Choix33->setScenario($Scenario9);
        $Choix33->setNom("Tenter de pacifier les créatures magiques qui protègent le Cristal de l'Aurore");
        $Choix33->setDescription("-");
        $Choix33->setPvPerdus(5);
        $Choix33->setPvGagnes(2);
        $Choix33->setEndurance(1);
        $Choix33->setPuissance(1);
        $Choix33->setIntelligence(2);
        $Choix33->setEsquive(2);

        // Choix 7 - Éclats Énigmatiques
        $Choix34 = new Choix();
        $Choix34->setScenario($Scenario9);
        $Choix34->setNom("Explorer les environs à la recherche d'éclats énigmatiques qui pourraient aider dans la quête");
        $Choix34->setDescription("-");
        $Choix34->setPvPerdus(5);
        $Choix34->setPvGagnes(2);
        $Choix34->setEndurance(1);
        $Choix34->setPuissance(0);
        $Choix34->setIntelligence(3);
        $Choix34->setEsquive(1);

        // Choix 8 - Conseils Sages
        $Choix35 = new Choix();
        $Choix35->setScenario($Scenario9);
        $Choix35->setNom("Demander des conseils aux sages du village sur la meilleure approche pour atteindre le Cristal");
        $Choix35->setDescription("-");
        $Choix35->setPvPerdus(5);
        $Choix35->setPvGagnes(1);
        $Choix35->setEndurance(2);
        $Choix35->setPuissance(0);
        $Choix35->setIntelligence(3);
        $Choix35->setEsquive(1);

        // Choix 9 - Harmonie Magique
        $Choix36 = new Choix();
        $Choix36->setScenario($Scenario10);
        $Choix36->setNom("Utiliser des compétences magiques pour rétablir l'harmonie entre les éclats de cristal");
        $Choix36->setDescription("-");
        $Choix36->setPvPerdus(5);
        $Choix36->setPvGagnes(2);
        $Choix36->setEndurance(1);
        $Choix36->setPuissance(2);
        $Choix36->setIntelligence(2);
        $Choix36->setEsquive(1);

        // Choix 10 - Accord Fragile
        $Choix37 = new Choix();
        $Choix37->setScenario($Scenario10);
        $Choix37->setNom("Tenter de négocier avec l'éclat perturbateur pour restaurer l'harmonie");
        $Choix37->setDescription("-");
        $Choix37->setPvPerdus(5);
        $Choix37->setPvGagnes(1);
        $Choix37->setEndurance(2);
        $Choix37->setPuissance(0);
        $Choix37->setIntelligence(2);
        $Choix37->setEsquive(2);

        // Choix 11 - Mélodie Céleste
        $Choix38 = new Choix();
        $Choix38->setScenario($Scenario10);
        $Choix38->setNom("Jouer une mélodie céleste pour calmer l'éclat rebelle");
        $Choix38->setDescription("-");
        $Choix38->setPvPerdus(5);
        $Choix38->setPvGagnes(2);
        $Choix38->setEndurance(1);
        $Choix38->setPuissance(1);
        $Choix38->setIntelligence(2);
        $Choix38->setEsquive(2);

        // Choix 12 - Intervention Énergique
        $Choix39 = new Choix();
        $Choix39->setScenario($Scenario10);
        $Choix39->setNom("Intervenir énergiquement pour contenir l'éclat perturbateur");
        $Choix39->setDescription("-");
        $Choix39->setPvPerdus(2);
        $Choix39->setPvGagnes(1);
        $Choix39->setEndurance(2);
        $Choix39->setPuissance(2);
        $Choix39->setIntelligence(0);
        $Choix39->setEsquive(1);

        // Choix 13 - Portail de l'Illusion
        $Choix40 = new Choix();
        $Choix40->setScenario($Scenario11);
        $Choix40->setNom("Entrer dans le portail de l'illusion, curieux de découvrir la réalité alternative");
        $Choix40->setDescription("-");
        $Choix40->setPvPerdus(2);
        $Choix40->setPvGagnes(0);
        $Choix40->setEndurance(1);
        $Choix40->setPuissance(0);
        $Choix40->setIntelligence(3);
        $Choix40->setEsquive(2);

        // Choix 14 - Porte des Énigmes
        $Choix41 = new Choix();
        $Choix41->setScenario($Scenario11);
        $Choix41->setNom("Choisir la porte des énigmes, prêt à résoudre les mystères pour avancer");
        $Choix41->setDescription("-");
        $Choix41->setPvPerdus(5);
        $Choix41->setPvGagnes(2);
        $Choix41->setEndurance(1);
        $Choix41->setPuissance(1);
        $Choix41->setIntelligence(3);
        $Choix41->setEsquive(2);

        // Choix 15 - Porte des Ombres
        $Choix42 = new Choix();
        $Choix42->setScenario($Scenario11);
        $Choix42->setNom("S'aventurer dans la porte des ombres, prêt à affronter les dangers cachés");
        $Choix42->setDescription("-");
        $Choix42->setPvPerdus(5);
        $Choix42->setPvGagnes(0);
        $Choix42->setEndurance(2);
        $Choix42->setPuissance(2);
        $Choix42->setIntelligence(0);
        $Choix42->setEsquive(1);

        // Choix 16 - Porte de la Clarté
        $Choix43 = new Choix();
        $Choix43->setScenario($Scenario11);
        $Choix43->setNom("Opter pour la porte de la clarté, espérant trouver une réalité plus lumineuse");
        $Choix43->setDescription("-");
        $Choix43->setPvPerdus(5);
        $Choix43->setPvGagnes(1);
        $Choix43->setEndurance(2);
        $Choix43->setPuissance(0);
        $Choix43->setIntelligence(3);
        $Choix43->setEsquive(1);

        // Choix 17 - Collection Cosmique
        $Choix44 = new Choix();
        $Choix44->setScenario($Scenario12);
        $Choix44->setNom("Collecter les étoiles filantes pour bénéficier de leurs énergies cosmiques");
        $Choix44->setDescription("-");
        $Choix44->setPvPerdus(2);
        $Choix44->setPvGagnes(0);
        $Choix44->setEndurance(1);
        $Choix44->setPuissance(2);
        $Choix44->setIntelligence(2);
        $Choix44->setEsquive(2);

        // Choix 18 - Éclipse Énigmatique
        $Choix45 = new Choix();
        $Choix45->setScenario($Scenario12);
        $Choix45->setNom("Explorer la source mystérieuse des étoiles filantes pour comprendre leur origine");
        $Choix45->setDescription("-");
        $Choix45->setPvPerdus(5);
        $Choix45->setPvGagnes(2);
        $Choix45->setEndurance(1);
        $Choix45->setPuissance(1);
        $Choix45->setIntelligence(3);
        $Choix45->setEsquive(2);

        // Choix 19 - Révélation Cosmique
        $Choix46 = new Choix();
        $Choix46->setScenario($Scenario12);
        $Choix46->setNom("Chercher une révélation cosmique en méditant sous la pluie d'étoiles filantes");
        $Choix46->setDescription("-");
        $Choix46->setPvPerdus(5);
        $Choix46->setPvGagnes(1);
        $Choix46->setEndurance(2);
        $Choix46->setPuissance(0);
        $Choix46->setIntelligence(3);
        $Choix46->setEsquive(1);

        // Choix 20 - Tempête Stellaire
        $Choix47 = new Choix();
        $Choix47->setScenario($Scenario12);
        $Choix47->setNom("Affronter la tempête stellaire et canaliser son énergie pour un pouvoir accru");
        $Choix47->setDescription("-");
        $Choix47->setPvPerdus(6);
        $Choix47->setPvGagnes(0);
        $Choix47->setEndurance(2);
        $Choix47->setPuissance(2);
        $Choix47->setIntelligence(0);
        $Choix47->setEsquive(1);

        // Choix 21 - Alliance Ancestrale
        $Choix48 = new Choix();
        $Choix48->setScenario($Scenario13);
        $Choix48->setNom("Formuler une alliance avec les Gardiens des Cristaux Perdus pour les aider dans leur quête");
        $Choix48->setDescription("-");
        $Choix48->setPvPerdus(5);
        $Choix48->setPvGagnes(2);
        $Choix48->setEndurance(1);
        $Choix48->setPuissance(1);
        $Choix48->setIntelligence(3);
        $Choix48->setEsquive(2);

        // Choix 22 - Poursuite Cristalline
        $Choix49 = new Choix();
        $Choix49->setScenario($Scenario13);
        $Choix49->setNom("Se lancer dans une quête effrénée à la recherche des cristaux perdus");
        $Choix49->setDescription("-");
        $Choix49->setPvPerdus(5);
        $Choix49->setPvGagnes(2);
        $Choix49->setEndurance(1);
        $Choix49->setPuissance(2);
        $Choix49->setIntelligence(2);
        $Choix49->setEsquive(2);

        // Choix 23 - Rituels Anciens
        $Choix50 = new Choix();
        $Choix50->setScenario($Scenario13);
        $Choix50->setNom("Explorer les rituels anciens pour comprendre la nature des cristaux perdus");
        $Choix50->setDescription("-");
        $Choix50->setPvPerdus(5);
        $Choix50->setPvGagnes(1);
        $Choix50->setEndurance(2);
        $Choix50->setPuissance(0);
        $Choix50->setIntelligence(3);
        $Choix50->setEsquive(1);

        // Choix 24 - Combat Épique
        $Choix51 = new Choix();
        $Choix51->setScenario($Scenario13);
        $Choix51->setNom("Engager un combat épique contre les créatures qui cherchent à s'approprier les cristaux");
        $Choix51->setDescription("-");
        $Choix51->setPvPerdus(6);
        $Choix51->setPvGagnes(0);
        $Choix51->setEndurance(2);
        $Choix51->setPuissance(2);
        $Choix51->setIntelligence(0);
        $Choix51->setEsquive(1);

        // Choix 25 - Purification Magique
        $Choix52 = new Choix();
        $Choix52->setScenario($Scenario14);
        $Choix52->setNom("Utiliser des pouvoirs magiques pour purifier la fontaine et chasser les ombres");
        $Choix52->setDescription("-");
        $Choix52->setPvPerdus(3);
        $Choix52->setPvGagnes(0);
        $Choix52->setEndurance(1);
        $Choix52->setPuissance(2);
        $Choix52->setIntelligence(2);
        $Choix52->setEsquive(2);

        // Choix 26 - Rituels de Clarté
        $Choix53 = new Choix();
        $Choix53->setScenario($Scenario14);
        $Choix53->setNom("Effectuer des rituels ancestraux pour restaurer la pureté de la fontaine");
        $Choix53->setDescription("-");
        $Choix53->setPvPerdus(5);
        $Choix53->setPvGagnes(2);
        $Choix53->setEndurance(1);
        $Choix53->setPuissance(1);
        $Choix53->setIntelligence(3);
        $Choix53->setEsquive(2);

        // Choix 27 - Confrontation Sombre
        $Choix54 = new Choix();
        $Choix54->setScenario($Scenario14);
        $Choix54->setNom("Affronter les ombres qui menacent la fontaine pour rétablir la clarté");
        $Choix54->setDescription("-");
        $Choix54->setPvPerdus(77);
        $Choix54->setPvGagnes(0);
        $Choix54->setEndurance(2);
        $Choix54->setPuissance(2);
        $Choix54->setIntelligence(0);
        $Choix54->setEsquive(1);

        // Choix 28 - Éclat Divin
        $Choix55 = new Choix();
        $Choix55->setScenario($Scenario14);
        $Choix55->setNom("Rechercher un éclat divin caché pour renforcer la puissance de la fontaine");
        $Choix55->setDescription("-");
        $Choix55->setPvPerdus(5);
        $Choix55->setPvGagnes(1);
        $Choix55->setEndurance(2);
        $Choix55->setPuissance(0);
        $Choix55->setIntelligence(3);
        $Choix55->setEsquive(1);

        $Choix56 = new Choix();
        $Choix56->setScenario($Scenario15);
        $Choix56->setNom("Ressentir la magie de l'éclat et suivre votre intuition");
        $Choix56->setDescription("-");
        $Choix56->setPvPerdus(3);
        $Choix56->setPvGagnes(0);
        $Choix56->setEndurance(1);
        $Choix56->setPuissance(1);
        $Choix56->setIntelligence(0);
        $Choix56->setEsquive(2);

        $Choix57 = new Choix();
        $Choix57->setScenario($Scenario15);
        $Choix57->setNom("Se renseigner auprès du Conseil Magique sur la signification de l'éclat");
        $Choix57->setDescription("-");
        $Choix57->setPvPerdus(5);
        $Choix57->setPvGagnes(1);
        $Choix57->setEndurance(1);
        $Choix57->setPuissance(0);
        $Choix57->setIntelligence(2);
        $Choix57->setEsquive(1);

        $Choix58 = new Choix();
        $Choix58->setScenario($Scenario15);
        $Choix58->setNom("Demander l'aide d'une licorne pour retrouver la trace de l'éclat");
        $Choix58->setDescription("-");
        $Choix58->setPvPerdus(5);
        $Choix58->setPvGagnes(0);
        $Choix58->setEndurance(1);
        $Choix58->setPuissance(0);
        $Choix58->setIntelligence(1);
        $Choix58->setEsquive(2);

        $Choix59 = new Choix();
        $Choix59->setScenario($Scenario15);
        $Choix59->setNom("Utiliser une carte magique pour localiser l'éclat dans Féélandia");
        $Choix59->setDescription("-");
        $Choix59->setPvPerdus(5);
        $Choix59->setPvGagnes(1);
        $Choix59->setEndurance(2);
        $Choix59->setPuissance(0);
        $Choix59->setIntelligence(2);
        $Choix59->setEsquive(1);

        // Choix pour le Scenario16
        $Choix60 = new Choix();
        $Choix60->setScenario($Scenario16);
        $Choix60->setNom("S'aventurer dans la Forêt des Lanternes pour retrouver la source des pleurs");
        $Choix60->setDescription("-");
        $Choix60->setPvPerdus(5);
        $Choix60->setPvGagnes(0);
        $Choix60->setEndurance(1);
        $Choix60->setPuissance(0);
        $Choix60->setIntelligence(2);
        $Choix60->setEsquive(1);

        $Choix61 = new Choix();
        $Choix61->setScenario($Scenario16);
        $Choix61->setNom("Demander à une fée de vous guider vers la source des pleurs");
        $Choix61->setDescription("-");
        $Choix61->setPvPerdus(5);
        $Choix61->setPvGagnes(1);
        $Choix61->setEndurance(2);
        $Choix61->setPuissance(0);
        $Choix61->setIntelligence(1);
        $Choix61->setEsquive(2);

        $Choix62 = new Choix();
        $Choix62->setScenario($Scenario16);
        $Choix62->setNom("Chercher des indices dans la vallée enneigée sur l'origine des pleurs");
        $Choix62->setDescription("-");
        $Choix62->setPvPerdus(5);
        $Choix62->setPvGagnes(1);
        $Choix62->setEndurance(1);
        $Choix62->setPuissance(0);
        $Choix62->setIntelligence(2);
        $Choix62->setEsquive(2);

        $Choix63 = new Choix();
        $Choix63->setScenario($Scenario16);
        $Choix63->setNom("Utiliser une boule de cristal magique pour localiser la source des pleurs");
        $Choix63->setDescription("-");
        $Choix63->setPvPerdus(5);
        $Choix63->setPvGagnes(2);
        $Choix63->setEndurance(1);
        $Choix63->setPuissance(0);
        $Choix63->setIntelligence(2);
        $Choix63->setEsquive(1);

        // Choix pour le Scenario17
        $Choix64 = new Choix();
        $Choix64->setScenario($Scenario17);
        $Choix64->setNom("Se rendre au Palais des Songes pour avertir la Reine des Fées");
        $Choix64->setDescription("-");
        $Choix64->setPvPerdus(3);
        $Choix64->setPvGagnes(0);
        $Choix64->setEndurance(2);
        $Choix64->setPuissance(0);
        $Choix64->setIntelligence(2);
        $Choix64->setEsquive(1);

        $Choix65 = new Choix();
        $Choix65->setScenario($Scenario17);
        $Choix65->setNom("Explorer la Caverne des Soupirs à la recherche d'indices");
        $Choix65->setDescription("-");
        $Choix65->setPvPerdus(5);
        $Choix65->setPvGagnes(0);
        $Choix65->setEndurance(1);
        $Choix65->setPuissance(0);
        $Choix65->setIntelligence(2);
        $Choix65->setEsquive(1);

        $Choix66 = new Choix();
        $Choix66->setScenario($Scenario17);
        $Choix66->setNom("Demander aux esprits de la forêt des conseils sur la situation");
        $Choix66->setDescription("-");
        $Choix66->setPvPerdus(5);
        $Choix66->setPvGagnes(1);
        $Choix66->setEndurance(2);
        $Choix66->setPuissance(0);
        $Choix66->setIntelligence(2);
        $Choix66->setEsquive(1);

        $Choix67 = new Choix();
        $Choix67->setScenario($Scenario17);
        $Choix67->setNom("Utiliser un sortilège de localisation pour trouver la Reine des Fées");
        $Choix67->setDescription("-");
        $Choix67->setPvPerdus(5);
        $Choix67->setPvGagnes(2);
        $Choix67->setEndurance(1);
        $Choix67->setPuissance(0);
        $Choix67->setIntelligence(1);
        $Choix67->setEsquive(2);

        // Choix pour le Scenario18
        $Choix68 = new Choix();
        $Choix68->setScenario($Scenario18);
        $Choix68->setNom("Participer à la fête enchantée pour apprendre davantage sur les rites féeriques");
        $Choix68->setDescription("-");
        $Choix68->setPvPerdus(5);
        $Choix68->setPvGagnes(2);
        $Choix68->setEndurance(1);
        $Choix68->setPuissance(0);
        $Choix68->setIntelligence(2);
        $Choix68->setEsquive(1);

        $Choix69 = new Choix();
        $Choix69->setScenario($Scenario18);
        $Choix69->setNom("Interroger les gardiens des portails magiques sur l'éclat disparu");
        $Choix69->setDescription("-");
        $Choix69->setPvPerdus(5);
        $Choix69->setPvGagnes(1);
        $Choix69->setEndurance(2);
        $Choix69->setPuissance(0);
        $Choix69->setIntelligence(1);
        $Choix69->setEsquive(2);

        $Choix70 = new Choix();
        $Choix70->setScenario($Scenario18);
        $Choix70->setNom("Consulter le Livre des Rêves pour trouver des réponses sur l'éclat");
        $Choix70->setDescription("-");
        $Choix70->setPvPerdus(5);
        $Choix70->setPvGagnes(2);
        $Choix70->setEndurance(1);
        $Choix70->setPuissance(0);
        $Choix70->setIntelligence(2);
        $Choix70->setEsquive(1);

        $Choix71 = new Choix();
        $Choix71->setScenario($Scenario18);
        $Choix71->setNom("Demander aux esprits des étoiles des conseils sur la mission");
        $Choix71->setDescription("-");
        $Choix71->setPvPerdus(5);
        $Choix71->setPvGagnes(1);
        $Choix71->setEndurance(2);
        $Choix71->setPuissance(0);
        $Choix71->setIntelligence(1);
        $Choix71->setEsquive(2);

        // Choix pour le Scenario19
        $Choix72 = new Choix();
        $Choix72->setScenario($Scenario19);
        $Choix72->setNom("Se lancer dans une quête pour retrouver les ailes volées");
        $Choix72->setDescription("-");
        $Choix72->setPvPerdus(5);
        $Choix72->setPvGagnes(0);
        $Choix72->setEndurance(1);
        $Choix72->setPuissance(0);
        $Choix72->setIntelligence(2);
        $Choix72->setEsquive(1);

        $Choix73 = new Choix();
        $Choix73->setScenario($Scenario19);
        $Choix73->setNom("Questionner le peuple des lutins sur la disparition des ailes");
        $Choix73->setDescription("-");
        $Choix73->setPvPerdus(5);
        $Choix73->setPvGagnes(1);
        $Choix73->setEndurance(2);
        $Choix73->setPuissance(0);
        $Choix73->setIntelligence(1);
        $Choix73->setEsquive(2);

        // Choix pour le Scenario19 (suite)
        $Choix74 = new Choix();
        $Choix74->setScenario($Scenario19);
        $Choix74->setNom("Explorer le Lac des Rêves pour retrouver des indices sur les voleurs d'ailes");
        $Choix74->setDescription("-");
        $Choix74->setPvPerdus(5);
        $Choix74->setPvGagnes(2);
        $Choix74->setEndurance(1);
        $Choix74->setPuissance(0);
        $Choix74->setIntelligence(2);
        $Choix74->setEsquive(1);

        $Choix75 = new Choix();
        $Choix75->setScenario($Scenario19);
        $Choix75->setNom("Faire appel aux sirènes du Lac des Rêves pour obtenir des visions sur les voleurs");
        $Choix75->setDescription("-");
        $Choix75->setPvPerdus(5);
        $Choix75->setPvGagnes(1);
        $Choix75->setEndurance(2);
        $Choix75->setPuissance(0);
        $Choix75->setIntelligence(1);
        $Choix75->setEsquive(2);

        $Choix76 = new Choix();
        $Choix76->setScenario($Scenario19);
        $Choix76->setNom("Demander l'aide des gardiens des rêves pour retrouver les ailes disparues");
        $Choix76->setDescription("-");
        $Choix76->setPvPerdus(5);
        $Choix76->setPvGagnes(2);
        $Choix76->setEndurance(1);
        $Choix76->setPuissance(0);
        $Choix76->setIntelligence(2);
        $Choix76->setEsquive(1);

        $Choix77 = new Choix();
        $Choix77->setScenario($Scenario19);
        $Choix77->setNom("Utiliser une boussole magique pour suivre la piste des voleurs d'ailes");
        $Choix77->setDescription("-");
        $Choix77->setPvPerdus(5);
        $Choix77->setPvGagnes(1);
        $Choix77->setEndurance(2);
        $Choix77->setPuissance(0);
        $Choix77->setIntelligence(1);
        $Choix77->setEsquive(2);

        // Choix pour le Scenario20
        $Choix78 = new Choix();
        $Choix78->setScenario($Scenario20);
        $Choix78->setNom("Plonger dans le Puits des Souvenirs pour retrouver les fragments de mémoire");
        $Choix78->setDescription("-");
        $Choix78->setPvPerdus(5);
        $Choix78->setPvGagnes(0);
        $Choix78->setEndurance(1);
        $Choix78->setPuissance(0);
        $Choix78->setIntelligence(2);
        $Choix78->setEsquive(1);

        $Choix79 = new Choix();
        $Choix79->setScenario($Scenario20);
        $Choix79->setNom("Questionner les gardiens des souvenirs sur la dispersion des fragments");
        $Choix79->setDescription("-");
        $Choix79->setPvPerdus(5);
        $Choix79->setPvGagnes(1);
        $Choix79->setEndurance(2);
        $Choix79->setPuissance(0);
        $Choix79->setIntelligence(1);
        $Choix79->setEsquive(2);

        $Choix80 = new Choix();
        $Choix80->setScenario($Scenario20);
        $Choix80->setNom("Faire appel aux esprits des rêves pour guider la recherche des fragments");
        $Choix80->setDescription("-");
        $Choix80->setPvPerdus(3);
        $Choix80->setPvGagnes(0);
        $Choix80->setEndurance(1);
        $Choix80->setPuissance(0);
        $Choix80->setIntelligence(2);
        $Choix80->setEsquive(1);

        $Choix81 = new Choix();
        $Choix81->setScenario($Scenario20);
        $Choix81->setNom("Utiliser une loupe magique pour repérer les fragments dans Féélandia");
        $Choix81->setDescription("-");
        $Choix81->setPvPerdus(5);
        $Choix81->setPvGagnes(1);
        $Choix81->setEndurance(2);
        $Choix81->setPuissance(0);
        $Choix81->setIntelligence(1);
        $Choix81->setEsquive(2);

        // Choix pour le Scenario21
        $Choix82 = new Choix();
        $Choix82->setScenario($Scenario21);
        $Choix82->setNom("S'aventurer dans le Royaume des Cauchemars pour retrouver le fragment obscur");
        $Choix82->setDescription("-");
        $Choix82->setPvPerdus(5);
        $Choix82->setPvGagnes(0);
        $Choix82->setEndurance(1);
        $Choix82->setPuissance(0);
        $Choix82->setIntelligence(1);
        $Choix82->setEsquive(2);

        $Choix83 = new Choix();
        $Choix83->setScenario($Scenario21);
        $Choix83->setNom("Demander l'aide des gardiens de l'équilibre pour trouver le fragment obscur");
        $Choix83->setDescription("-");
        $Choix83->setPvPerdus(5);
        $Choix83->setPvGagnes(1);
        $Choix83->setEndurance(2);
        $Choix83->setPuissance(0);
        $Choix83->setIntelligence(2);
        $Choix83->setEsquive(1);

        $Choix84 = new Choix();
        $Choix84->setScenario($Scenario21);
        $Choix84->setNom("Explorer les méandres de la Bibliothèque des Songes pour des indices");
        $Choix84->setDescription("-");
        $Choix84->setPvPerdus(5);
        $Choix84->setPvGagnes(2);
        $Choix84->setEndurance(1);
        $Choix84->setPuissance(0);
        $Choix84->setIntelligence(2);
        $Choix84->setEsquive(1);

        $Choix85 = new Choix();
        $Choix85->setScenario($Scenario21);
        $Choix85->setNom("Utiliser un cristal de révélation pour éclairer le chemin vers le fragment obscur");
        $Choix85->setDescription("-");
        $Choix85->setPvPerdus(2);
        $Choix85->setPvGagnes(0);
        $Choix85->setEndurance(2);
        $Choix85->setPuissance(0);
        $Choix85->setIntelligence(1);
        $Choix85->setEsquive(2);

        // Choix pour le Scenario22
        $Choix86 = new Choix();
        $Choix86->setScenario($Scenario22);
        $Choix86->setNom("Se lancer dans une quête pour retrouver le joyau éclaté");
        $Choix86->setDescription("-");
        $Choix86->setPvPerdus(5);
        $Choix86->setPvGagnes(2);
        $Choix86->setEndurance(1);
        $Choix86->setPuissance(0);
        $Choix86->setIntelligence(1);
        $Choix86->setEsquive(2);

        $Choix87 = new Choix();
        $Choix87->setScenario($Scenario22);
        $Choix87->setNom("Demander aux gardiens des émotions des conseils pour retrouver le joyau éclaté");
        $Choix87->setDescription("-");
        $Choix87->setPvPerdus(5);
        $Choix87->setPvGagnes(1);
        $Choix87->setEndurance(2);
        $Choix87->setPuissance(0);
        $Choix87->setIntelligence(2);
        $Choix87->setEsquive(1);

        $Choix88 = new Choix();
        $Choix88->setScenario($Scenario22);
        $Choix88->setNom("Explorer les champs de fleurs pour trouver des pétales liés au joyau éclaté");
        $Choix88->setDescription("-");
        $Choix88->setPvPerdus(3);
        $Choix88->setPvGagnes(0);
        $Choix88->setEndurance(1);
        $Choix88->setPuissance(0);
        $Choix88->setIntelligence(2);
        $Choix88->setEsquive(1);

        $Choix89 = new Choix();
        $Choix89->setScenario($Scenario22);
        $Choix89->setNom("Interroger les esprits des fleurs sur la localisation du joyau éclaté");
        $Choix89->setDescription("-");
        $Choix89->setPvPerdus(5);
        $Choix89->setPvGagnes(1);
        $Choix89->setEndurance(2);
        $Choix89->setPuissance(0);
        $Choix89->setIntelligence(1);
        $Choix89->setEsquive(2);

        // Pour les Planètes
        $manager->persist($Planete1);
        $manager->persist($Planete2);
        $manager->persist($Planete3);

        // Pour les Scénarios
        $manager->persist($Scenario2);
        $manager->persist($Scenario3);
        $manager->persist($Scenario4);
        $manager->persist($Scenario5);
        $manager->persist($Scenario6);
        $manager->persist($Scenario7);
        $manager->persist($Scenario8);
        $manager->persist($Scenario9);
        $manager->persist($Scenario10);
        $manager->persist($Scenario11);
        $manager->persist($Scenario12);
        $manager->persist($Scenario13);
        $manager->persist($Scenario14);
        $manager->persist($Scenario15);
        $manager->persist($Scenario16);
        $manager->persist($Scenario17);
        $manager->persist($Scenario18);
        $manager->persist($Scenario19);
        $manager->persist($Scenario20);
        $manager->persist($Scenario21);
        $manager->persist($Scenario22);

        // Pour les Choix
        $manager->persist($Choix4);
        $manager->persist($Choix5);
        $manager->persist($Choix6);
        $manager->persist($Choix7);
        $manager->persist($Choix8);
        $manager->persist($Choix9);
        $manager->persist($Choix10);
        $manager->persist($Choix11);
        $manager->persist($Choix12);
        $manager->persist($Choix13);
        $manager->persist($Choix14);
        $manager->persist($Choix15);
        $manager->persist($Choix16);
        $manager->persist($Choix17);
        $manager->persist($Choix18);
        $manager->persist($Choix19);
        $manager->persist($Choix20);
        $manager->persist($Choix21);
        $manager->persist($Choix22);
        $manager->persist($Choix23);
        $manager->persist($Choix24);
        $manager->persist($Choix25);
        $manager->persist($Choix26);
        $manager->persist($Choix27);
        $manager->persist($Choix28);
        $manager->persist($Choix29);
        $manager->persist($Choix30);
        $manager->persist($Choix31);
        $manager->persist($Choix32);
        $manager->persist($Choix33);
        $manager->persist($Choix34);
        $manager->persist($Choix35);
        $manager->persist($Choix36);
        $manager->persist($Choix37);
        $manager->persist($Choix38);
        $manager->persist($Choix39);
        $manager->persist($Choix40);
        $manager->persist($Choix41);
        $manager->persist($Choix42);
        $manager->persist($Choix43);
        $manager->persist($Choix44);
        $manager->persist($Choix45);
        $manager->persist($Choix46);
        $manager->persist($Choix47);
        $manager->persist($Choix48);
        $manager->persist($Choix49);
        $manager->persist($Choix50);
        $manager->persist($Choix51);
        $manager->persist($Choix52);
        $manager->persist($Choix53);
        $manager->persist($Choix54);
        $manager->persist($Choix55);
        $manager->persist($Choix56);
        $manager->persist($Choix57);
        $manager->persist($Choix58);
        $manager->persist($Choix59);
        $manager->persist($Choix60);
        $manager->persist($Choix61);
        $manager->persist($Choix62);
        $manager->persist($Choix63);
        $manager->persist($Choix64);
        $manager->persist($Choix65);
        $manager->persist($Choix66);
        $manager->persist($Choix67);
        $manager->persist($Choix68);
        $manager->persist($Choix69);
        $manager->persist($Choix70);
        $manager->persist($Choix71);
        $manager->persist($Choix72);
        $manager->persist($Choix73);
        $manager->persist($Choix74);
        $manager->persist($Choix75);
        $manager->persist($Choix76);
        $manager->persist($Choix77);
        $manager->persist($Choix78);
        $manager->persist($Choix79);
        $manager->persist($Choix80);
        $manager->persist($Choix81);
        $manager->persist($Choix82);
        $manager->persist($Choix83);
        $manager->persist($Choix84);
        $manager->persist($Choix85);
        $manager->persist($Choix86);
        $manager->persist($Choix87);
        $manager->persist($Choix88);
        $manager->persist($Choix89);

        $manager->flush();

    }
}
